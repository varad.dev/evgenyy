<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Evgeny Yakushev | Home</title>
    <link rel="stylesheet" href="assets/css/bds.css">
    <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
    <style media="screen">
    a.anchor {
        display: block;
        position: relative;
        top: -100px;
        visibility: hidden;
    }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        // Check for click events on the navbar burger icon
        $(".navbar-burger").click(function() {
            // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
            $(".navbar-burger").toggleClass("is-active");
            $(".navbar-menu").toggleClass("is-active");
            $(".navbar-end > .navbar-item").children().first().toggleClass("is-inverted");
        });
    });
    </script>
</head>
<body class="has-navbar-fixed-top">
    <!-- Navbar Start -->
    <nav class="navbar is-fixed-top is-black is-spaced"  role="navigation" aria-label="main navigation">
        <div class="navbar-brand" style="flex-grow: 1">
            <a class="navbar-item" href="/" data-scroll="top">
                <img src="assets/logo/SVG/logo-light.svg" alt="" height="48" width="48">
            </a>
            <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                <span aria-hidden="true"></span>
                <span aria-hidden="true" class="is-hidden"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>
        <div  class="navbar-menu">
            <div class="navbar-start">
                <a href="#about"class="navbar-item has-text-weight-medium is-active" data-scroll="about">
                    About
                </a>
                <a href="#services" class="navbar-item has-text-weight-medium" data-scroll="services">
                    Services
                </a>
                <a href="#contact" class="navbar-item has-text-weight-medium" data-scroll="contact">
                    Contact
                </a>
            </div>
            <div class="navbar-end">
                <div class="navbar-item">

                    <!-- <a class="button is-rounded is-light is-outlined is-small">
                    <strong>Hire Me</strong>
                </a> -->
            </div>
        </div>
    </div>
</nav>
<!-- Navbar End -->
<section class="section" id="top">
    <div class="container">
        <div class="level is-size-8 has-background-white-bis has-text-dark">
            <div class="columns is-vcentered">
                <div class="column" style="order:2">
                    <div class="level-item ">
                        <div class="box is-large is-size-5 ">
                            <div class="card-content">

                                <h1 class="title is-spaced  is-2 is-family-secondary">Evgeny Yakushev</h1>
                                <hr class="is-size-3">
                                Since 2017, my journey has led me from being an intern in a startup to being a representative of a large investor syndicate.
                                <br>
                                <br>
                                Today, I am an experienced investment analyst with a track record of successful investments for various associates in venture capital and angel syndicates. My expertise helps companies develop sustainable ways to grow both financially and strategically.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column is-4">
                    <div class="level-item ">
                        <div class="box is-large is-paddingless">
                            <figure class="image is-1by1" >
                                <img  src="/assets/images/PROFILE.png">
                            </figure>
                            <div class="card is-large is-shadowless">
                                <div class="card-content is-size-7">
                                    <i class="icon ion-ios-person has-text-dark is-medium"></i>
                                    <span>
                                        <span class="has-text-grey-dark ">
                                            Name:
                                        </span>
                                        <strong>Evgeny Yakushev</strong>
                                    </span>
                                    <br>
                                    <i class="icon ion-ios-map has-text-dark is-medium"></i>
                                    <span><span class="has-text-grey-dark">
                                        Location:
                                    </span>
                                    <strong>San Francisco, US</strong></span>
                                    <br>
                                    <i class="icon ion-ios-beer has-text-dark is-medium"></i>
                                    <span><span class="has-text-grey-dark">
                                        Date Of Birth:
                                    </span>
                                    <strong>June 14th 1996</strong></span>
                                    <br>
                                    <i class="icon ion-ios-send has-text-dark is-medium"></i>
                                    <span><span class="has-text-grey-dark">
                                        Mail:
                                    </span>
                                    <strong>mail@evgenyy.com</strong></span>
                                    <br>
                                    
                                </div>
                                <div class="card-footer is-borderless ">
                                    <a href="https://www.linkedin.com/in/evgenyyakushev/" class="card-footer-item is-borderless"><i class="icon ion-logo-linkedin is-size-3  has-text-dark"></i></a>
                                    <a href="https://www.facebook.com/eevgeny.yakushev" class="card-footer-item is-borderless"><i class="icon ion-logo-facebook is-size-3 has-text-dark"></i></a>
                                    <a href="https://www.instagram.com/evgeny_yakushev/" class="card-footer-item is-borderless"><i class="icon ion-logo-instagram is-size-3 has-text-dark"></i></a>
                                    <!-- <a href="#" class="card-footer-item is-borderless"><i class="icon ion-logo-googleplus is-size-3 has-text-dark"></i></a> -->
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section" >
    <a class="anchor" id="about"></a>
    <div class="container">
        <p class="title is-2 is-family-secondary has-text-centered">
            <u>About
            </u>
        </p>
        <p>
            Growing up in an international environment, I have developed a unique multicultural approach to thinking, which helps me find solutions to rising problems in a non-traditional way.<br><br>

            Since graduating from Hult International Business School in 2019 with a Bachelor’s degree in Finance, I have been actively involved with startup and investor ecosystems in Silicon Valley. Since 2017, my journey led me from being an intern in a startup to representative of a large investor syndicate. Today, I am an experienced investment analyst with a track record of successful investments for various associates in venture capital and angel syndicates. My expertise helps companies develop sustainable ways to grow both financially and strategically. I also act as an advisor to several international startups and lecture on venture capital and investor communication both in San Francisco and Moscow.<br><br>

            I am also a published author. While in college, I participated as a member of an editorial board for the <a href="https://www.cabrillo.edu/publications/portergulch/">Porter Gulch Review 2015</a> - an annual journal collecting local poetry and prose of Santa Cruz. Later, I joined the editorial board for the <a href="https://investinfra.ru/biblioteka-lidera/berega-i-gorizonty.html">Shores and Horizons: Current Asset Management Practices</a>, an in-depth study of asset management practices in the PPP (Public - Private Partnerships) sector. My focus was on the international scene, especially on the asset management and allocation strategies for endowment funds at Harvard, Yale, Stanford and the Nobel Foundation. <br><br>

            Since my early years, I was fascinated with the ocean, which is why I became a scuba-diver. I obtained my professional license at age 18 and completed over 80 dives in Mediterranean Sea, Atlantic and Indian oceans. Some of my other interests and passions include sustainability activism, international history, black tea, and outdoor sports like soccer.<br><br>

            In regards to my sustainability activism, I am a strong advocate for the PRI - Principles for Responsible Investment. I believe that the most significant impact on the environment and the society can be achieved only through the financial sector, e.g. heavy investment into companies researching aging-related diseases or complete divestment from carbon-emitting productions and technologies.
        </p>
        <hr class="is-size-2">
        <div class="columns has-background-white-bis is-paddingless is-marginless">
            <div class="column is-offset-1"  style="order:2">
                <hr class="is-size-2">
                <p class="title is-4 is-family-secondary has-text-centered">
                    Education
                </p>
                <div class="columns is-paddingless is-marginless">
                    <div class="column is-narrow ">
                        <p class="title is-6 has-text-grey-darker">2019</p>
                    </div>
                    <div class="column is-2 has-text-centered">
                        <figure class="image is-64x64 is-inline-block" >
                            <img  src="/assets/images/hult.png">
                        </figure>
                    </div>
                    <div class="column">
                        <div class="box is-paddingless is-marginless" >
                            <p class="title is-5">Hult International Business School</p>
                            <p class="subtitle is-7">Bachelor of Business Administration - BBA, Finance </p>
                        </div>
                    </div>
                </div>
                <hr class="is-size-4">
                <div class="columns is-paddingless is-marginless">
                    <div class="column is-narrow ">
                        <p class="title is-6 has-text-grey-darker">2017</p>
                    </div>
                    <div class="column is-2 has-text-centered">
                        <figure class="image is-48x48 is-inline-block" >
                            <img  src="/assets/images/cabrillo.png">
                        </figure>
                    </div>
                    <div class="column">
                        <div class="box is-paddingless is-marginless" >
                            <p class="title is-5">Cabrillo College</p>
                            <p class="subtitle is-7">Business Administration and Management, General</p>
                        </div>
                    </div>
                </div>
                <hr class="is-size-4">
                <div class="columns is-paddingless is-marginless">
                    <div class="column is-narrow ">
                        <p class="title is-6 has-text-grey-darker">2014</p>
                    </div>
                    <div class="column is-2 has-text-centered">
                        <figure class="image is-64x64 is-inline-block" >
                            <img  src="/assets/images/aloha.png">
                        </figure>
                    </div>
                    <div class="column">
                        <div class="box is-paddingless is-marginless" >
                            <p class="title is-5">Aloha College</p>
                            <p class="subtitle is-7">International Baccalaureate</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="column is-paddingless is-marginless">
                <div class="card is-large">
                    <div class="card-content has-background-black-bis has-text-white ">
                        <hr class="is-size-3">
                        <p class="title is-4 is-family-secondary has-text-centered has-text-white">
                            Experience
                        </p>
                        <div class="columns is-paddingless is-marginless">
                            <div class="column is-3 ">
                                <p class="title is-6 has-text-grey-light">2019-Present</p>
                            </div>
                            <div class="column">
                                <div class="box is-paddingless is-marginless" >
                                    <p class="title is-5 has-text-white">American Pillar - International IR Group</p>
                                    <p class="subtitle is-7 has-text-white">Associate Director, Client Relations</p>
                                </div>
                            </div>
                        </div>
                        <div class="columns is-paddingless is-marginless">
                            <div class="column is-3 ">
                                <p class="title is-6 has-text-grey-light">2018-2019</p>
                            </div>
                            <div class="column">
                                <div class="box is-paddingless is-marginless" >
                                    <p class="title is-5 has-text-white">Bay Angels</p>
                                    <p class="subtitle is-7 has-text-white">Relationship Manager</p>
                                </div>
                            </div>
                        </div>
                        <div class="columns is-paddingless is-marginless">
                            <div class="column is-3 ">
                                <p class="title is-6 has-text-grey-light">2017-2019</p>
                            </div>
                            <div class="column">
                                <div class="box is-paddingless is-marginless" >
                                    <p class="title is-5 has-text-white">Pension & Actuarial Consulting</p>
                                    <p class="subtitle is-7 has-text-white">Responsible Investment Analyst</p>
                                </div>
                            </div>
                        </div>
                        <div class="columns is-paddingless is-marginless">
                            <div class="column is-3 ">
                                <p class="title is-6 has-text-grey-light">2018</p>
                            </div>
                            <div class="column">
                                <div class="box is-paddingless is-marginless" >
                                    <p class="title is-5 has-text-white">Investinfra</p>
                                    <p class="subtitle is-7 has-text-white">Research Analyst</p>
                                </div>
                            </div>
                        </div>
                        <div class="columns is-paddingless is-marginless">
                            <div class="column is-3 ">
                                <p class="title is-6 has-text-grey-light">2018</p>
                            </div>
                            <div class="column">
                                <div class="box is-paddingless is-marginless" >
                                    <p class="title is-5 has-text-white">The Vault</p>
                                    <p class="subtitle is-7 has-text-white">Assistant General Manager</p>
                                </div>
                            </div>
                        </div>
                        <div class="columns is-paddingless is-marginless">
                            <div class="column is-3 ">
                                <p class="title is-6 has-text-grey-light">2016</p>
                            </div>
                            <div class="column">
                                <div class="box is-paddingless is-marginless" >
                                    <p class="title is-5 has-text-white">Astro Digital</p>
                                    <p class="subtitle is-7 has-text-white">Internship</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="is-size-2">
        <p class="title is-3 is-family-secondary has-text-centered">
            Skills
        </p>
        <div class="columns">
            <div class="column has-text-centered">
                <div class="box">
                    <p class="title is-5">Financial Planning</p>
                    <progress class="progress is-small is-success" value="85" max="100">95%</progress>
                </div>
            </div>
            <div class="column has-text-centered">
                <div class="box">
                    <p class="title is-5">Start-ups</p>
                    <progress class="progress is-small is-success" value="100" max="100">90%</progress>
                </div>
            </div>
            <div class="column has-text-centered">
                <div class="box">
                    <p class="title is-5">Business Strategy</p>
                    <progress class="progress is-small is-success" value="95" max="100">90%</progress>
                </div>

            </div>
            <div class="column has-text-centered">
                <div class="box">
                    <p class="title is-5">Project Management</p>
                    <progress class="progress is-small is-success" value="95" max="100">85%</progress>
                </div>
            </div>


        </div>
    </div>
</section>
<section class="section has-background-white-bis" >
    <a class="anchor" id="services"></a>
    <div class="container">
        <p class="title is-2 is-family-secondary has-text-centered">
            <u>Services
            </u>
        </p>
        <hr class="is-size-5">
        <div class="columns">
            <div class="column has-text-centered">
                <div class="box is-white hover-to-popping">
                    <figure class="image is-64x64 is-inline-block">
                        <img src="/assets/svg/fa.svg">
                    </figure>
                    <br>
                    <br>
                    <p class="title is-5">Financial Planning</p>
                    <div class="expanded-content">
                        My finance background provides me with a skillset required for conducting a comprehensive financial planning and analysis tools that can help early stage companies with the initial planning and review of their activities. I can also consult on the financial aspects of the fundraising process.

                    </div>
                </div>
            </div>
            <div class="column has-text-centered ">
                <div class="box is-white hover-to-popping">
                    <figure class="image is-64x64 is-inline-block">
                        <img src="/assets/svg/ba.svg" >
                    </figure>
                    <br>
                    <br>
                    <p class="title is-5">Business Analytics</p>
                    <div class="expanded-content">
                        I produce in-depth company reviews, due diligence reports, and other forms of researches for asset management and research companies. Some of my work can be found on <a href="https://investinfra.ru/biblioteka-lidera/berega-i-gorizonty.html">Shores and Horizons: Current Asset Management Practices</a> and on Medium.
                    </div>
                </div>
            </div>
            <div class="column has-text-centered">
                <div class="box is-white hover-to-popping">
                    <figure class="image is-64x64 is-inline-block">
                        <img src="/assets/svg/sc.svg">
                    </figure>
                    <br>
                    <br>
                    <p class="title is-5">Startup Councelling</p>
                    <div class="expanded-content">
                        My expertise allowed me to create an insightful framework that helps startups achieve their current goals. With my assistance, you will be able to identify weaknesses and overcome them to get ready for the fundraising stage. I would be excited to help your company accelerate growth.
                    </div>
                </div>
            </div>
            <div class="column has-text-centered">
                <div class="box is-white hover-to-popping">
                    <figure class="image is-64x64 is-inline-block">
                        <img src="/assets/svg/se.svg">
                    </figure>
                    <br>
                    <br>
                    <p class="title is-5">Startup Evaluation</p>
                    <div class="expanded-content">
                        Being a great company from an investment perspective requires a lot of strategic thinking and storytelling skills. Throughout my career I helped dozens of companies with their pitches, negotiations and other investor communications.
                    </div>
                </div>
            </div>
            <div class="column has-text-centered">
                <div class="box is-white hover-to-popping">
                    <figure class="image is-64x64 is-inline-block">
                        <img src="/assets/svg/es.svg">
                    </figure>
                    <br>
                    <br>
                    <p class="title is-5">Event Speaker</p>
                    <div class="expanded-content">
                        I am always happy to come to your event as a speaker for a panel related to entrepreneurship or judging startup pitches. My focus is on blockchain, fintech and some areas of healthtech applications.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section" >
    <a class="anchor" id="contact"></a>
    <div class="container">
        <p class="title is-2 is-family-secondary has-text-centered">
            <u>Contact
            </u>
        </p>
        <hr>
        <form class="box is-well is-large is-marginless" action="https://formspree.io/xdogywan" method="POST">
            <div class="columns">
                <div class="column">
                    <label for="name" class="label">Name</label>
                    <input id="name" name="name" class="input is-rounded" type="text" placeholder="Fantastic Person" required>
                </div>
                <div class="column">
                    <label for="email" class="label">Email</label>
                    <input id="email" class="input is-rounded" type="email" placeholder="fantastic-person@place.tld" name="_replyto" required>
                </div>
            </div>

            <label for="subject" class="label">Subject</label>
            <input id="subject" name="subject" class="input is-rounded" type="text" placeholder="What do you want to talk about?" required>
            <hr>
            <label class="label">Message</label>
            <textarea name="message" class="textarea is-rounded" placeholder="Pour your heart out... " required></textarea>
            <hr>
            <div class="control">
                <button class="button is-link">Submit</button>
            </div>
        </form>
    </div>

</section>
<section class="section">
    <footer class="footer">
        <div class="content has-text-centered">
            <p>
                <strong>Designed</strong>  by <a href="https://varad.dev">varad.dev</a>
            </p>
        </div>
    </footer>
</section>

<script>
/*
Funtion to toggle class for expandable box. Enter in documentation.
Use jquery and save this as backup.
*/

// Array.from(document.getElementsByClassName("is-expandable")).forEach(function(element) {
//     element.addEventListener('click', function (){
//         let box = document.getElementById(element.id);
//         box.classList.toggle("is-expanded");
//         //box.classList.toggle("is-well");
//         //box.classList.toggle("is-popping");
//     });
// });

$(window).scroll(function() {
    var windscroll = $(window).scrollTop();
    if (windscroll >= $(window).height()) {
        $('body section').each(function(i) {
            if ($(this).position().top <= windscroll+100) {
                $('nav .navbar-menu .navbar-start a.is-active').removeClass('is-active');
                $('nav .navbar-menu .navbar-start a').eq(i).addClass('is-active');
            }
        });
    } else {
        $('nav .navbar-menu .navbar-start a.is-active').removeClass('is-active');
        $('nav .navbar-brand a:first').addClass('is-active');
    }

}).scroll();

function openModal(num)
{
    let modal = document.getElementById("js-modal"+num);
    modal.classList.add("is-active");
}

function closeModal(num)
{
    let modal = document.getElementById("js-modal"+num);
    modal.classList.remove("is-active");
}
</script>

</body>
</html>
